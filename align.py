#!/usr/bin/env python

import os
import gzip
import shutil
import subprocess

from pathlib import Path


def gunzip(file: Path) -> Path:
    with gzip.open(file, 'rb') as f_in:
        with open(os.path.splitext(file)[0], 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
            return Path(f_out.name)


#####################################################
###          Input variables from ENV             ###
#####################################################

# read environment variables

genome_folder = Path(os.getenv("genome_folder"))
star_index_folder = genome_folder.joinpath("star_index")
run_thread = os.getenv("max_threads")
max_memory = int(os.getenv("max_memory"))
max_memory *= 1024 * 1024  # convert memory from MB to bytes
track_name_0 = os.getenv('track_name_0_0')
track_name_1 = os.getenv('track_name_0_1')
protocol = int(os.getenv("PROTOCOL", 2))
is_paired_end = protocol == 2

# get selected splice junction database
sjdb = os.getenv("SJDB")
sjdb_file = Path(os.getenv("SJDB_FILE", ""))

#########################################################
###   Build the parameters and the command string     ###
#########################################################
print("Starting alignment")

cmd_list = ["STAR", "--genomeDir", star_index_folder.as_posix()]

# access the reads via input_i_s with 'i' denoting the input node index and 's' denoting the sample index
# the activity is parallelized across samples, thus, in the paired-end mode the index of the first read 0 and the second is 1

if is_paired_end:
    cmd_list.extend(["--readFilesIn", os.getenv("input_0_0"), os.getenv('input_0_1')])
else:
    cmd_list.extend(["--readFilesIn", os.getenv('input_0_0')])

# get the description based on the track names
if is_paired_end:
    out_file_name_prefix = os.path.commonprefix([track_name_0, track_name_1])
else:
    out_file_name_prefix = track_name_0

cmd_list.extend([
    "--readFilesCommand", "gunzip -c -d",
    "--outFileNamePrefix", out_file_name_prefix,
    "--outSAMunmapped", "Within",
    "--outSAMtype", "BAM", "SortedByCoordinate",
    "--outSAMattributes", "All",
    "--genomeLoad", "NoSharedMemory",
    "--runThreadN", run_thread
])

if sjdb == "true":
    cmd_list.extend(["--sjdbGTFfile", gunzip(sjdb_file)])

if max_memory > 0:
    cmd_list.extend([
        "--limitGenomeGenerateRAM", str(max_memory),
        "--limitBAMsortRAM", str(max_memory)
    ])

exit_code = subprocess.call(cmd_list)

# publish the result file to the output node at index 0
# the output node can later access this file via input_0_0 in the present case
# in general, the i-th output node can access teh j-th file via input_i_j
if exit_code == 0:
    bam_file = out_file_name_prefix + "Aligned.sortedByCoord.out.bam"
    shutil.move(bam_file, "0_0_output.bam")

exit(exit_code)
