#!/usr/bin/env python

import os
import subprocess
import re
import gzip
import shutil

from pathlib import Path


def ensure_unzipped(file: Path) -> Path:
    if re.match("\.(fa|fas|fasta|fna)\.(gz)", "".join(file.suffixes)):
        with gzip.open(file, 'rb') as f_in:
            with open(os.path.splitext(file)[0], 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
                return Path(f_out.name)
    else:
        return file


#####################################################
###          Input variables from ENV             ###
#####################################################

# read environment variables, for instance use 'max_threads' to pass the number of required threads
# from the activity.xml to STAR

genome_folder = Path(os.getenv('genome_folder'))
assembly_name = os.getenv('genome_assembly_name')
star_index_folder_name = "star_index"
star_index_folder = genome_folder.joinpath(star_index_folder_name)
run_thread = os.getenv('max_threads')
max_memory = int(os.getenv('max_memory'))
max_memory *= 1024 * 1024  # convert memory from MB to bytes

#########################################################
###     Check for index and create if necessary       ###
#########################################################

# Build STAR index for reference genome selected in the workflow's import activity

exit_code = 0

if not star_index_folder.exists():
    print(f"Creating STAR index in {genome_folder.as_posix()}")
    # ensure that fasta files are unzipped
    paths = [ensure_unzipped(file).as_posix()
             for file in genome_folder.iterdir()
             if file.match("*.f*")]
    if len(paths) == 0:
        raise f"Error. No reference genome files found in {genome_folder.as_posix()}!"
    # the STAR build command is a list of strings that can be later passed to subprocess.call
    # we append options to the list as needed
    cmd_list = [
        "STAR",
        "--runMode", "genomeGenerate",
        "--genomeDir", star_index_folder_name,
        "--runThreadN", run_thread,
        "--outFileNamePrefix", star_index_folder_name,
        "--genomeSAindexNbases", "9"
    ]
    cmd_list.extend(["--genomeFastaFiles"] + sorted(paths))

    if max_memory > 0:
        cmd_list.extend(["--limitGenomeGenerateRAM", str(max_memory)])

    # execute STAR and build the genome indices
    exit_code = subprocess.call(cmd_list)

    if exit_code == 0:
        shutil.copytree("star_index", star_index_folder)

exit(exit_code)
