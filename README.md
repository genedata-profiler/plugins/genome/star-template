# STAR Template Plugin

This repository contains a minimal example of a custom plugin for the Genome workflow module in Genedata Profiler.
The plugin performs NGS read alignment using he RNA-seq alignment tool STAR.

The following quickstart provides the essential steps for running the plugin in the Profiler environment. For the 
complete documentation, open the Profiler documentation and navigate to **Getting Started** &rarr; **Developer Guides** 
&rarr; **Plugin Development**. Details about the plugin deployment can be found at **Getting Started** 
&rarr; **Developer Guides** &rarr; **Python API & CLI Quickstart**.

We recommend cloning this repository into a VSCode session in the Profiler Analytics Environment. The following steps
assume that you are in the root folder of the project.

# Quickstart

This section describes how to use Docker to manage the required libraries and tools used by the plugin, first and 
foremost the actual STAR aligner.  

1. In the Profiler Gitlab environment [create a GitLab access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)
   with the scopes `read_registry` and ``write_registry`` enabled. Store the access token in an environment variable 
   called `REGISTRY_ACCESS_TOKEN`. We also set the URL to the private container registry in Gitlab and the image name as 
   shown below, and we can then build and publish the Docker image as follows: 
   ```shell
   REGISTRY_ACCESS_TOKEN="your GitLab access token"
   REGISTRY_HOST="your-profiler-gitlab.com:8443"
   REGISTRY_PATH="path/to/docker/registry/project"
   IMAGE_NAME="star-template"

   docker login -u $USER -p $REGISTRY_ACCESS_TOKEN $REGISTRY_HOST
   docker build -t "$REGISTRY_HOST/$REGISTRY_PATH/$IMAGE_NAME" docker
   docker push "$REGISTRY_HOST/$REGISTRY_PATH/$IMAGE_NAME"
   ```   
2. Finally, we adapt the `activity.xml` file to use our new image. This file is the main file of our plugin, where all 
   commands, inputs, and outputs are specified. Therefore, set the image attribute in the command tags to the actual 
   Docker image URL. This ensures that the plugin uses the image at runtime and our main scripts are executed in the 
   right environment.
3. Now that the code is ready, we can deploy our plugin to Profiler using the Genedata Profiler CLI. To install the 
   Profiler CLI, execute the following command:
   ```bash
   pip install genedata-profiler-api
   ```
4. To use the CLI, we need to configure and login once into Profiler by calling following commands. For the final 
   authentication, you will be asked for your Profiler credentials:
   ```bash
   gdp config set server_url <server-url>
   gdp config set client_id <client-id>
   gdp config set client_secret <client-secret>
   gdp credentials save
   ```
5. Now upload the STAR Template plugin into Profiler's Genome workflow module using the command below. We also provide 
   the Docker registry access token.
   ```bash
   gdp plugins upload --module Genome --force-overwrite --docker-username $USER --docker-password $REGISTRY_ACCESS_TOKEN .
   ```
6. To try the plugin, you can navigate to a study in the Profiler web application and start a new Genome Workflow 
   session. In the workflow editor, drag-and-drop `Import Unaligned Reads` and the newly deployed `STAR Template` plugin 
   activity to the workflow pane and execute the workflow. You can use example data from [Zenodo](https://zenodo.org), 
   e.g., [this macrophage study](https://zenodo.org/record/7636289).
